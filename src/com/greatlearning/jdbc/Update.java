package com.greatlearning.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Update {

	public static void main(String[] args) {
		String dbURL = "jdbc:mysql://localhost:3306/jdbc_demo?useSSL=false";
		String username = "root";
		String password = "welcome";

		try (Connection conn = DriverManager.getConnection(dbURL, username, password)) {

			String sql = "UPDATE employees SET email=?  WHERE name=?";

			PreparedStatement statement = conn.prepareStatement(sql);
			statement.setString(1, "harish@outlook.com");
			statement.setString(2, "harish");

			int rowsUpdated = statement.executeUpdate();
			if (rowsUpdated > 0) {
				System.out.println("An existing user was updated successfully!");
			}


		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
}