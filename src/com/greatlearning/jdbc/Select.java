package com.greatlearning.jdbc;


import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Select {

	public static void main(String[] args) {
		String dbURL = "jdbc:mysql://localhost:3306/jdbc_demo?useSSL=false";
		String username = "root";
		String password = "welcome";
		
		try (Connection conn = DriverManager.getConnection(dbURL, username, password)) {
			
			String sql = "SELECT * FROM employees";
			
			Statement statement = conn.createStatement();
			ResultSet result = statement.executeQuery(sql);
			
			int count = 0;
			
			while (result.next()){
				long id = result.getLong(1);
				String name = result.getString(2);
				String email = result.getString(3);
				Date dob = result.getDate(4);
				
				System.out.println("Employee "+"Id:: "+ id+ "name :: "+ name + " email :: "+ email + " dob: "+dob);
			}
			
		} catch (SQLException ex) {
			ex.printStackTrace();
		}		
	}
}