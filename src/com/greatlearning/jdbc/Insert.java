package com.greatlearning.jdbc;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;


public class Insert {

	public static void main(String[] args) {
		String dbURL = "jdbc:mysql://localhost:3306/jdbc_demo?useSSL=false";
		String username = "root";
		String password = "welcome";
		
		/*
		 * Steps to connect to the database
		 * 
		 * 1. Create the connection object - Pass the username and password
		 * 2. Create a Prepared statement
		 * 3. Execute the statement
		 * 4. Close the statement
		 * 5. Close the connection
		 */
		PreparedStatement statement = null;
		try (Connection conn = DriverManager.getConnection(dbURL, username, password)) {
			
			String sql = "INSERT INTO employees (name, email, dob) VALUES (?, ?, ?)";
			
			statement = conn.prepareStatement(sql);
			statement.setString(1, "Vikram");
			statement.setString(2, "vikram@gmail.com");
			statement.setDate(3, Date.valueOf(LocalDate.of(1982, 10, 16)));
			
			int rowsInserted = statement.executeUpdate();
			if (rowsInserted > 0) {
				System.out.println("A new user was inserted successfully!");
			}

			
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}